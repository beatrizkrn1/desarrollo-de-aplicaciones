package sentenciawhile;

/**
 *
 * @author Beatriz Karina Duran Siqueiros
 * Asignatura: Desarrollo de Aplicaciones
 * Semipresencial
 * Grupo: ITI 9-3
 */
public class SentenciaWHILE {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
       int x = 0;
        while(x<=10){
            System.out.println("Valor es: "+x);
            x = x+2;
        }
            
    }
    
}