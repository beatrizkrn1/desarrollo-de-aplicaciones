package arreglos;

/**
 *
 * @author Beatriz Karina Duran Siqueiros
 * Asignatura: Desarrollo de Aplicaciones
 * Semipresencial
 * Grupo: ITI 9-3
 */
public class Arreglos {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int x[] = {10,5,7};
      
        /*x[0] = 10;
        x[1] = 5;
        x[2] = 7;
        x[3] = 2;*/
         for (int i=0;i<x.length;i++){
             System.out.println("Arreglo en el índice: "+i+" Es igual a: "+x[i]);
         }
    }
    
}