package clasesobjetosmetodos;

/**
 *
 * @author Beatriz Karina Duran Siqueiros
 * Asignatura: Desarrollo de Aplicaciones
 * Semipresencial
 * Grupo: ITI 9-3
 */

public class Carro {
    double peso;
double altura;
double ancho;
double largo;
int numeroPuertas;
boolean encendido=false;
String modelo;
    
public Carro(){
    this.peso = 1000;
    this.altura = 1.90;
    this.ancho = 4;
}
public double obtenerPeso(){
    return this.peso;
}  
public void encender(){
    this.encendido = true;
    System.out.println("El carro esta encendido");
}
public void apagar(){
    this.encendido = false;
}
}