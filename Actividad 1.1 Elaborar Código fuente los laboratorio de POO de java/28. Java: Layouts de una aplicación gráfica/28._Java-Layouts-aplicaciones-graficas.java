package javalayoutsaplicaciongrafica;

/**
 *
 * @author luis duran
 */
public class JavaLayoutsAplicacionGrafica {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Ventana2 v = new Ventana2();
        v.setVisible(true);
        v.setBounds(200, 200, 400, 250);
    }
    
}


//CLASE VENTANA2

package javalayoutsaplicaciongrafica;
import javax.swing.*;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.FlowLayout;
/**
 *
 * @author luis duran
 */
public class Ventana2 extends JFrame{
    private JPanel panel1;
    private JPanel panel2;
    private JButton b1,b2,b3,b4,b5,b6,b7,b8,b9;
    public Ventana2()
    {
        panel1 = new JPanel();
        panel2 = new JPanel();
        
        b1 =  new JButton("Prueba");
        b2 =  new JButton("Prueba");
        b3 =  new JButton("Prueba");
        b4 =  new JButton("Prueba");
        b5 =  new JButton("Prueba");
        b6 =  new JButton("Prueba");
        b7 =  new JButton("Prueba");
        b8 =  new JButton("Prueba");
        b9 =  new JButton("Prueba");
                
        this.add(panel1,BorderLayout.NORTH);
        this.add(panel2,BorderLayout.SOUTH);  
        
        panel2.add(b1);
        panel2.add(b2);
        panel2.add(b3);
        panel2.add(b4);
        panel2.add(b5);
        panel2.add(b6);
        panel2.add(b7);
        panel1.add(b8);
        panel1.add(b9);
    }
}