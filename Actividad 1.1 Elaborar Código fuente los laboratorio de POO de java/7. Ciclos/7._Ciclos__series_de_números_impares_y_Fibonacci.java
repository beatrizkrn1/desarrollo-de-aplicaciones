package ciclosseriesnumerosimparesfibonacci;

/**
 *
 * @author Beatriz Karina Duran Siqueiros
 * Asignatura: Desarrollo de Aplicaciones
 * Semipresencial
 * Grupo: ITI 9-3
 */
public class CiclosSeriesNumerosImparesFibonacci {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int x = 1;
        int anterior = 0;
        int temp;
        while(true){
            System.out.println(x);
            temp = x;
            x=x+anterior;
            anterior=temp;
            if(x>100){
                break;
            }
        }
    }