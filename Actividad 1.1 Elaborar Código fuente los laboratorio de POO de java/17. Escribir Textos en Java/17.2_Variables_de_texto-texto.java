package escribirtextos;
import java.io.*;
/**
 *
 * @author luis duran
 */
public class texto {
    public void escribir(String nombreArchivo){
            File f = new File(nombreArchivo);
            
            try{
                FileWriter w = new FileWriter(f);
                BufferedWriter bw = new BufferedWriter(w);
                PrintWriter wr = new PrintWriter(bw);
                
                 wr.write("Esto es una lines de texto");
                 wr.write("Esto es una concatenación al texto");
                 wr.close();
                 wr.close();
            }catch (Exception ex){
                
            }
    }
    public void leer(String nombreArchivo){
        try{
            FileReader r = new FileReader(nombreArchivo);
            BufferedReader buffer = new BufferedReader(r);
            
            System.out.println(buffer.readLine());
            System.out.println(buffer.readLine());
            String temp="";
            while(temp!=null){
                temp = buffer.readLine();
                if(temp==null)
                    break;
                System.out.println(temp);
            }
            
            System.out.println(buffer.readLine());
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
}