package ordenamientoburbuja;

/**
 *
 * @author luis duran
 */
public class OrdenamientoBurbuja {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        double []arreglo = {5,3,4,2,67,8,12,9,23,4,5};
        Ordenador o = new Ordenador();
        o.ordenarBurbuja(arreglo);
        
        for(int i=0;i<arreglo.length;i++){
            System.out.println(arreglo[i]);
        }
    }
    
}



//CLASE ORDENADOR

package ordenamientoburbuja;

/**
 *
 * @author luis duran
 */
public class Ordenador {
    public void ordenarBurbuja(double [] array){
        double aux;
        boolean cambios=false;
        
        while(true){
            cambios=false;
            for(int i=1; i<array.length;i++)
            {
                if(array[i]<array[i-1])
                {
                    aux=array[i];
                    array[i] = array[i-1];
                    array[i-1] = aux;
                    cambios=true;
                }
            }
            if(cambios==false)
                break;
        }
    }
}