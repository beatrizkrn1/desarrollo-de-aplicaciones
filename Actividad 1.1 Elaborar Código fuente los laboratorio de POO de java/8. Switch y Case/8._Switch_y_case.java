package switchcase;

/**
 *
 * @author Beatriz Karina Duran Siqueiros
 * Asignatura: Desarrollo de Aplicaciones
 * Semipresencial
 * Grupo: ITI 9-3
 */
public class SwitchCase {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int x;
        x='a';
        switch(x){
            case 'a':{
                 System.out.println("Esta es la opción 0");
            break;
            }
            case 'b':{
                System.out.println("Esta es la opción 1");
                break; 
            }
        }
    }
    
}