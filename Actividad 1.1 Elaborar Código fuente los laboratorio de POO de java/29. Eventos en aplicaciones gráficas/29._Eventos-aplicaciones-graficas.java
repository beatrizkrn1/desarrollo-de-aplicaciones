package javalayoutsaplicaciongrafica;

/**
 *
 * @author luis duran
 */
public class JavaLayoutsAplicacionGrafica {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Ventana4 v = new Ventana4();
        v.setVisible(true);
        v.setBounds(200, 200, 400, 250);
    }
    
}


//CLASE VENTANA4
package javalayoutsaplicaciongrafica;
import javax.swing.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
/**
 *
 * @author luis duran
 */
public class Ventana4 extends JFrame implements ActionListener{
    JPanel panel;
    JButton boton;
    JLabel texto;
    JTextField input;
    public Ventana4(){
        panel = new JPanel();
        boton = new JButton("Oprimir");
        texto = new JLabel();
        input = new JTextField("Escribir aqui");
        
        this.add(panel);
        
        panel.add(boton);
        panel.add(input);
        panel.add(texto);
        
        this.setDefaultCloseOperation(this.EXIT_ON_CLOSE);
        
        this.boton.addActionListener(this);
    }
    public void actionPerformed(ActionEvent e){
        this.texto.setText(this.input.getText());
    }
}
