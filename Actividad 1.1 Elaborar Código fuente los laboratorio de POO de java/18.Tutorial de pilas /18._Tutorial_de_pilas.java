package tutorial.de.pilas;
import java.util.Stack;
/**
 *
 * @author luis duran
 */
public class TutorialDePilas {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Stack pila = new Stack();
        pila.push(50);
        pila.push("String");
        pila.push(17);
        pila.push("Palabra");
        
        System.out.println("El último elemento de la pila es: "+pila.peek());
        while(pila.empty()==false){
            System.out.println(pila.pop());
        }
    }
    
}
