package colas;
import java.util.LinkedList;
/**
 *
 * @author luis duran
 */
public class Colas {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        //FIFO first.in, first out
        
        LinkedList cola =  new LinkedList();
        
        for(int i=1; i<11;i++){
            cola.offer(i);
        }
        while(cola.peek()!=null){
            System.out.println(cola.poll());
        }
    }
    
}