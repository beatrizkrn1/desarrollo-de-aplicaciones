/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prototype;

/**
 *
 * @author luis duran
 */
public abstract class MedioTransporte implements Cloneable {
    protected String motor;
    protected String color;
    protected String marca;
    protected String modelo;
    protected TipoAlimentacion alimentacion;
    protected String nombre;
    protected int cantidadPasajeros;
    
    public MedioTransporte(String motor, String color, String marca, 
            String modelo, TipoAlimentacion alimentacion, String nombre, 
            int cantidadPasajeros){
        this.motor = motor;
        this.color = color;
        this.marca = marca;
        this.modelo = modelo;
        this.nombre = nombre;
        this.cantidadPasajeros = cantidadPasajeros;
        this.alimentacion = alimentacion;
    }
    
    public MedioTransporte clonar() throws CloneNotSupportedException{
        return (MedioTransporte) super.clone();
    }
    
    public abstract String toString();
    
    public void setColor(String color){
        this.color = color;
    }
}
