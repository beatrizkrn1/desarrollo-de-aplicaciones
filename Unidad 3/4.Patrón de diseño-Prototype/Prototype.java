/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prototype;
import static prototype.TipoAlimentacion.GASOLINA;
/**
 *
 * @author luis duran
 */
public class Prototype {
    /**
     * @param args the command line arguments
     */
   
    public static void main(String[] args){
     try{
     Terrestre carro = new Terrestre("VVL", "Negro", "Nissan", "1998", GASOLINA,
     "Carro", 5, 4);
     System.out.println("Carro nuevo \n"+carro.toString());
     Terrestre clon = (Terrestre) carro.clonar();
     clon.setColor("Azul");
     System.out.println("Carro clonado \n"+ clon.toString());
     }catch(CloneNotSupportedException error){
            System.out.println(error.getMessage());
     }
     
    }  
}