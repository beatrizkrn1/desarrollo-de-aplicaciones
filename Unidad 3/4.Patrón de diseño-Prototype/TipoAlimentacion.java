/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prototype;

/**
 *
 * @author luis duran
 */
public enum TipoAlimentacion {
    GASOLINA, DIESEL, ELECTRICO, AGUA, NINGUNO;
    
    public String toString(TipoAlimentacion tipo){
        switch(tipo){
            case GASOLINA:
                return "Gasolina";
            case DIESEL:
                return "Diesel";
            case ELECTRICO:
                return "Eléctrico";
            case AGUA:
                return "Agua";
            case NINGUNO:
                return "Ninguno";
            default:
                return "";
        }
    }
}
