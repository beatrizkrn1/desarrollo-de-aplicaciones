/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prototype;

/**
 *
 * @author luis duran
 */
public class Terrestre extends MedioTransporte{
    private int cantidadLlantas;
    
    public Terrestre(String motor, String color, String marca, String modelo, 
            TipoAlimentacion alimentacion, String nombre, int cantidadPasajeros,
            int cantidadLlantas){
        super(motor, color, marca, modelo, alimentacion, nombre, cantidadPasajeros);
        this.cantidadLlantas = cantidadLlantas;
    }
    
    public String toString(){
        String s = "Nombre: " + this.nombre + "\n";
        s += "Tipo de alimenatacion: "+this.alimentacion.toString(alimentacion)+"\n";  
        s += "Motor: "+this.motor+"\n";
        s += "Color: "+this.color+"\n";
        s += "Marca: "+this.marca+"\n";
        s += "Modelo: "+this.modelo+"\n";
        s += "Cantidad de pasajeros: "+this.cantidadPasajeros+"\n";
        s += "Cantidad de llantas: "+this.cantidadLlantas+"\n";
        return s;
    }
}
