/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package factorymethod;

/**
 *
 * @author luis duran
 */

//AQUI SE IMPPLEMENTA EL PATRON DE DISEÑO FACTOR METHOD EL CUAL
//INTEGRA LO QUE ES EL IVA Y EL IVA REDUCIDO HACIENDO UNA VALIDACIÓN.
public class FactoriaFacturas {
    public static Factura getFactura(String tipo) {
    if (tipo.equals("iva")) {
        return new facturaIva();
    }
    else {
        return new FacturaIvaReducido();
    }
 }
}
