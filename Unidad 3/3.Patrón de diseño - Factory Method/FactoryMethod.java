/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package factorymethod;

/**
 *
 * @author luis duran
 */
public class FactoryMethod {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // SE CREA EL OBJETO DE LA FACTURA, LA CUAL HACE USO DE LA CLASE 
        //FACTORIAFACTURAS, LEYENDO EL ID Y EL IMPORTE, POSTERIORMENTE 
        //SE IMPRIME LO QUE ES EL IMPORTE JUNTO CON EL IVA.
        Factura f= FactoriaFacturas.getFactura("iva");
        f.setId(1);
        f.setImporte(100);
        System.out.println(f.getImporteIva());
    }
    
}
