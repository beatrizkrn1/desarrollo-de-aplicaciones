/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package factorymethod;

/**
 *
 * @author luis duran
 */

//ESTA CLASE ABSTRACTA PERMITE TENER LOS METODOS DEL IMPORTE Y DEL IMPORTE CON 
//IVA QUE SERÁN UTILIZADOS EN OTRAS CLASES COMO FACTURAIVA Y FACTURAIVAREDUCIDO.
public abstract class Factura {
    private int id;
    private double importe;
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public double getImporte() {
        return importe;
    }
    public void setImporte(double importe) {
        this.importe = importe;
    }
    public abstract double getImporteIva();
}
