/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package factorymethod;

/**
 *
 * @author luis duran
 */

//AQUI SE REDUCE EL IVA LA CUAL UTILIZA LA CLASE DE FACTURA. DE IGUAL FORMA 
//HACE EL USO DEL MÉTODO IMPORTE IVA, REGRESANDO LO QUE ES EL IMPORTE.
public class FacturaIvaReducido extends Factura{
    public double getImporteIva() {
    // TODO Auto-generated method stub
    return getImporte()*1.07;
 }
}
