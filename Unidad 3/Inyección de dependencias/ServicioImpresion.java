/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inyecciondependencias2;

/**
 *
 * @author luis duran
 */
public class ServicioImpresion {
    ServicioEnvio servicioA;
    ServicioPDF servicioB;
  
    public ServicioImpresion() {
    
    this.servicioA= new ServicioEnvio();
    this.servicioB= new ServicioPDF();
  }
    public void imprimir() {
    servicioA.enviar();
    servicioB.pdf();
  }
}
