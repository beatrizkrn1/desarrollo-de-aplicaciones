/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pdsingleton;
import pdsingleton.Marcianos;
/**
 *
 * @author luis duran
 */

//ESTA CLASE SE ENCARGA DE DESTRUIR MARCIANOS.
public class Jugador {
    private Marcianos marcianos;
    
    public Jugador(){
        marcianos = Marcianos.getMarcianos();
    }
    //MÉTODO PARA DESTRUIR MARCIANOS
    public void destruirMarciano(){
        marcianos.derribarMarcianos();
    }
}
