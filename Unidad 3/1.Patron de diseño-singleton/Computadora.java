/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pdsingleton;
import pdsingleton.Marcianos;

/**
 *
 * @author luis duran
 */

//ESTA CLASE SE CREA CADA VEZ QUE EL MARCIANO DISPARE
public class Computadora {
   //SE CREA EL MÉTODO MARCIANOS
    private Marcianos marcianos;
   //CONSTRUCTOR DE LA COMPUTADORA
    public Computadora(){
         marcianos = Marcianos.getMarcianos();
    }
    //LLAMANDO AL MÉTODO PARA CREAR MARCIANOS
    public void creaMarcianos(){
        marcianos.creaMarcianos();
    }
}
