/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pdsingleton;
import java.util.Scanner;
/**
 *
 * @author luis duran
 */
public class PDSingleton {
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
    Marcianos marcianos;
    Computadora computadora;
    Jugador jugador;
    Scanner sc;
        marcianos = Marcianos.getMarcianos();
        computadora = new Computadora();
        jugador = new Jugador();  
        sc = new Scanner(System.in);
        
        int disparos;
        //CICLO EN EL CUAL EL USUARIO VA INGRESAR LA VECES QUE VA DISPARAR
        //Y EL FOR INDICARÁ CUANTOS MARCIANOS MATÓ, DEPENDIENDO SI CREO MARCIANOS.2
        
        do{
            System.out.println("Digite los disparos:");
            disparos = sc.nextInt();
            for (int i = 0; i < disparos; i++) {
                jugador.destruirMarciano();
            }
            computadora.creaMarcianos();
        }while(disparos!=0);
    }   
}
