/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pdsingleton;

/**
 *
 * @author luis duran
 */
public final class Marcianos {
    private static final Marcianos marcianos = new Marcianos();
    //CREACIÓN DE LA VARIABLE PARA GUARDAR LA CANTIDAD DE MARCIANOS
    private static int cantidad;    
    
    //CREACIÓN DEL CONSTRUCTOR
    private Marcianos(){
        //CANTIDAD DE MARCIANOS
        cantidad = 10;
    }
    //MÉTODO EL CUAL SE VA UTILIZAR EN LAS DEMÁS CLASES Y PERMITIRÁ AUMENTAR LA
    //CANTIDAD DE MARCIANOS O DISMINUIRÁ.
    public static Marcianos getMarcianos(){
        return marcianos;
    }
    
    //VALIDAR QUE EXISTAN MÁS DE 0 MARCIANOS
    public static void derribarMarcianos(){
        System.out.println("Soy el bueno, derribe un marciano");
        if(cantidad>0) cantidad--;
    }
    
    //CLASE CREAR MARCIANOS
    public static void creaMarcianos(){
        System.out.println("Soy el malo, cree un marciano");
        if(cantidad>0) cantidad++;
    }
    
    //CUANTOS QUEDAN
   public static void cuantosQuedan(){
       if(cantidad>0){
           System.out.println("Quedan en el juego:" +cantidad+" marcianos");
       }else{
           System.out.println("Felicidades, has ganado!!!");
       }
   }
}
