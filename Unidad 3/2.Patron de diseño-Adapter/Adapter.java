/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adapter;
import java.util.Scanner;

/**EL PRINCIPAL OBJETIVO DE ESTE PATRÓN ES PODER ADAPTAR UN OBJETO
 *A OTRO, CON EL FIN DE PODER UTILIZAR LAS CARACTERÍSTICAS DEL OBJETO
 * ORIGINAL EN UN AMBIENTE DIFERENTE.
 * @author luis duran
 */
public class Adapter {
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner sc = new Scanner(System.in);
        Nombre objetoNombre = new Nombre();
        
        System.out.println();
        System.out.println();
        System.out.print("Digite su nombre y apellido:");
        objetoNombre.setNombreCompuesto(sc.nextLine());
        
        System.out.println();
        System.out.println();
        System.out.println("Tu nombre completo es: " +objetoNombre.getNombreCompuesto());
    }
    
}
