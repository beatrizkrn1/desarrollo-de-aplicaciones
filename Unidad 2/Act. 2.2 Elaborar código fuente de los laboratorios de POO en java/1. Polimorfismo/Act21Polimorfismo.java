/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package act.pkg2.pkg1polimorfismo;

/**
 *
 * @author luis duran
 */
public class Act21Polimorfismo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic herea
        //En este código se visualiza como se integra en un array
        //LO DE LA CLASE
        B[] bs = new B[2];
        bs[0] = new B();
        bs[1] = new A();
        
        naivePrinter(bs);
    }
    private static void naivePrinter(B[] bs){
        //ESTE FOR RECORRRE TODO EL MÉTODO
        for (int i = 0; i < bs.length; i++) {
            //AQUI SE IMPRIME CADA ELEMENTO DEL ARRAY
            bs[i].print();
        }
    }
    
}
