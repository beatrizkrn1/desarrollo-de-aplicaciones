package interfacesvisuales;
import javax.swing.*;
import java.awt.event.*;

/**
 *
 * @author luis duran
 */
public class InterfacesVisuales extends JFrame implements ActionListener{
    private JTextField textfield1;
    private JLabel label1;
    private JButton boton1;
    public InterfacesVisuales() {
       setLayout(null);
        label1=new JLabel("Usuario:");
        label1.setBounds(10,10,100,30);
        add(label1);
        textfield1=new JTextField();
        textfield1.setBounds(120,10,150,20);
        add(textfield1);
        boton1=new JButton("Aceptar");
        boton1.setBounds(10,80,100,30);
        add(boton1);
        boton1.addActionListener(this);
}
 public void actionPerformed(ActionEvent e) {
        if (e.getSource()==boton1) {
            String cad=textfield1.getText();
            setTitle(cad);
        }
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        InterfacesVisuales for1=new InterfacesVisuales();
        for1.setBounds(10,10,400,300);
        for1.setVisible(true);
        for1.setResizable(false);
    }
    
}