package interfacesvisuales;
import javax.swing.*;
import java.awt.event.*;

/**
 *
 * @author luis duran
 */
public class InterfacesVisuales extends JFrame{
    private JScrollPane scrollpane1;
    private JTextField textfield1;
    private JTextArea textarea1;
    public InterfacesVisuales() {
       setLayout(null);
        textfield1=new JTextField();
        textfield1.setBounds(10,10,200,30);
        add(textfield1);
        textarea1=new JTextArea();
        textarea1.setBounds(10,50,400,300);
        add(textarea1);
         scrollpane1=new JScrollPane(textarea1); 
         scrollpane1.setBounds(10,50,400,300);
          add(scrollpane1);
}
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        InterfacesVisuales for1=new InterfacesVisuales();
        for1.setBounds(10,10,400,300);
        for1.setVisible(true);
        for1.setResizable(false);
    }
    
}