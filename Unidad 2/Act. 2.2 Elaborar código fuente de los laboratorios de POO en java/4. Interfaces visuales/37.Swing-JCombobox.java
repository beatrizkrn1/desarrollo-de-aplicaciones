package interfacesvisuales;
import javax.swing.*;
import java.awt.event.*;

/**
 *
 * @author luis duran
 */
public class InterfacesVisuales extends JFrame implements ItemListener{
    private JComboBox combo1;
    public InterfacesVisuales() {
       setLayout(null);
        combo1=new JComboBox();
        combo1.setBounds(10,10,80,20);
        add(combo1);
        combo1.addItem("rojo");
        combo1.addItem("vede");
        combo1.addItem("azul");
        combo1.addItem("amarillo");
        combo1.addItem("negro");
        combo1.addItemListener(this);
}
     public void itemStateChanged(ItemEvent e) {
        if (e.getSource()==combo1) {
            String seleccionado=(String)combo1.getSelectedItem();
            setTitle(seleccionado);
        }
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        InterfacesVisuales for1=new InterfacesVisuales();
        for1.setBounds(10,10,400,300);
        for1.setVisible(true);
        for1.setResizable(false);
    }
    
}
