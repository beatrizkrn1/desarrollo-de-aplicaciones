/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package act2.pkg1.interfaces;

/**
 *
 * @author luis duran
 */
public class Act21Interfaces {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // SE CREA EL OBJETO DE CARRO MEDIANTE LA CLASE CARRO
        Carro c = new Carro();
        Bicicleta b = new Bicicleta();
        
        //SE LLAMAN LOS METODOS DE LAS CLASES CON LA INTERFAZ IMPLEMENTADA.
        c.avanzar();
        b.avanzar();
    }
    
}
