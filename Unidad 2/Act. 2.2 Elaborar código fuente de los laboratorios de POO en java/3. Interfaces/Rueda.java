/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package act2.pkg1.interfaces;

/**
 *
 * @author luis duran
 */

//ESTE ARCHIVO SE CREO SIENDO DE TIPO "INTERFACE"
//LA DIFERENCIA DE UNA CLASE, ES QUE DICE INTERFACE, MÁS NO CLASS.
public interface Rueda {

public int valor = 50;
//SE CREAN DOS MÉTODOS avanzar y detenerse.
    public void avanzar();
   
    
    public void detenerse();
}
