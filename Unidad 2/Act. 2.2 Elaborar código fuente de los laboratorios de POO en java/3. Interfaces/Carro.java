/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package act2.pkg1.interfaces;

/**
 *
 * @author luis duran
 */

//EN ESTE MÉTODO SE UTILIZA LA PALABRA "IMPLEMENTS" PARA IMPLEMENTAR 
// UNA INTERFAZ, EN ESTE CASO LLAMADA "RUEDA".
// CUANDO APARECEN EN COLOR ROJO EL NOMBRE DE LA CLASE,
//ES NECESARIO IMPLEMENTAR LOS MÉTODOS DE LA INTERFACE "RUEDA".
public class Carro implements Rueda{
    //SE CREA UN COSNTRUCTOR
    public Carro(){
        
    }
    
    public void avanzar(){
        System.out.println("El carro avanza");
    }
    public void detenerse(){
        System.out.println("El carro se detiene");
    }
}
