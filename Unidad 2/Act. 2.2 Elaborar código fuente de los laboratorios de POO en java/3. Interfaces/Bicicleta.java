/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package act2.pkg1.interfaces;

/**
 *
 * @author luis duran
 */

//LA CLASE BICICLETA TAMBIÉN SE MUEVE POR MEDIO DE LAS
//RUEDAS.
public class Bicicleta implements Rueda{
 //SE CREA UN CONSTRUCTOR   
public Bicicleta(){
    
}    

//SE IMPLEMENTAN LOS METODOS.
    public void avanzar(){
        System.out.println("La bicicleta avanza");
    }
    public void detenerse(){
        System.out.println("La bicicleta se detiene");
    }
}
