/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clasesabstractas;

/**
 *
 * @author luis duran
 */

//PARA QUE UNA CLASE SEA ABSTRACTA HAY QUE AGREGAR A LA CLASE
//"ABSTRACT"
public abstract class Animal {
   
    //Se crea la variable de tipo STRING y PRIVATE.
    private String nombre;
    //LUEGO SE CREA EL CONSTRUCTOR
    public Animal(){}
    //ESTE METOPDO SE AGREGA COMO LA CARACTERISCA DEL ANIMALITO.
    //EL CUAL IMPRIME UN COMENTARIO SOBRE EL MISMO.
    public void comer(){
    System.out.println("El "+nombre+ " esta comiendo");
    }
    
    //CARACTERISTICAS DEL ANIMALITO
    public abstract void moverse();
    
//SE CREA EL MÉTODO PARA SABER EL NOMBRE DEL ANIMAL.
    public void setNombre(String s){
        nombre = s;
    }
    
    public String getNombre(){
        return nombre;
    }
}  

