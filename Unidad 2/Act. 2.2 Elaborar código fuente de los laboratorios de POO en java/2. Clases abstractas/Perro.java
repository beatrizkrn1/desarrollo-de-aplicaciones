/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clasesabstractas;

/**
 *
 * @author luis duran
 */

//HEREDA LO DE LA CLASE ANIMAL (EXTENDS)
public class Perro extends Animal {
    //SE IMPLEMENTA EL MÉTOPDO DE CONSTRUCTOR
   public Perro(){
       //SE EJCUTA EL CONSTRUCTOR DE LA CLASE PADRE QUE SE 
       //LOCALIZA EN EL CLASE ANIMAL.
       super();
       setNombre("Perro");
   }
    

//SE DECLARA ESTÁ METODO POR QUE CORRRESPONDE A LA CLASE ANIMAL
    //YA QUE SE IMPLEMENTA EL CODIGO YA NO MARCA ERROR.
    public void moverse(){
        System.out.println("El perro se mueve");
    }
}
