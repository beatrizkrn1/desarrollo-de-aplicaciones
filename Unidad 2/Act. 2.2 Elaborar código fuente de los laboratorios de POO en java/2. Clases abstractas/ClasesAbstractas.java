/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clasesabstractas;

/**
 *
 * @author luis duran
 */
public class ClasesAbstractas {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //SE CREA EL OBJETO LLAMADO "P" DE LA CLASE "PERRO".
        Perro p = new Perro();
       //EL PERRO SE PUEDE MOVER.
        p.comer();
        //EL PERRO COMER.
        p.moverse();
    }
    
}
